# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: 'Bertram', email: 'bertram@example.com',
                     password: 'thisisatest', password_confirmation: 'thisisatest')
  end

  # Testuser basicalliy valid
  test 'user should be vaild' do
    assert @user.valid?
  end

  # Name validation tests
  test 'name should be present' do
    @user.name = '   '
    assert_not @user.valid?
  end
  test 'name shouldn\'t be to long' do
    @user.name = 'a' * 51
    assert_not @user.valid?
  end

  # E-mail validation tests
  test 'email should be present' do
    @user.email = '   '
    assert_not @user.valid?
  end
  test 'email shouldn\'t be to long' do
    @user.email = 'a' * 244 + '@example.com'
    assert_not @user.valid?
  end
  test 'email should be valid' do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  test 'email shouldn\'t be valid' do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?
    end
  end
  test 'email adresses should be unique' do
    duplicated_user = @user.dup
    @user.save
    assert_not duplicated_user.valid?
  end

  # Password validation tests
  test 'password should be present (non blank)' do
    @user.password = @user.password_confirmation = '    '
    assert_not @user.valid?
  end
  test 'password should have minimum length' do
    @user.password = @user.password_confirmation = 'a' * 7
    assert_not @user.valid?
  end

  test 'authenticated? should return false for a user with nil digest' do
    assert_not @user.authenticated?('')
  end
end
