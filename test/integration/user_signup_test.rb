require 'test_helper'

class UserSignupTest < ActionDispatch::IntegrationTest
  test 'invalid signup information' do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name: '',
                                         email: 'boris@example',
                                         password: 'match',
                                         password_valdation: 'nomatch' } }
    end
    assert_template 'users/new'
    assert_select   'div#error_explanation'
    assert_select   'div.field_with_errors'
  end

  test 'valid signup information' do
    get signup_path
    assert_difference 'User.count' do
      post users_path, params: { user: { name: 'Karl',
                                         email: 'karl@example.com',
                                         password: 'test1234',
                                         password_valdation: 'test1234' } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert_not flash.empty?
    assert is_logged_in?
  end
end
